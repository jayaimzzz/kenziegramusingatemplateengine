const express = require("express");
const multer = require("multer");
const fs = require("fs");

const port = 3000;
const publicPath = ('public/')

const app = express();
app.use(express.json());
app.use(express.static(publicPath));
app.set('views', './views');
app.set('view engine', 'pug');
const upload = multer({
    dest: publicPath + "uploads/"
});

const uploadedFiles = [];

app.post('/upload', upload.single('my-file'), function (request, response, next) {
    // request.file is the `myFile` file
    // request.body will hold the text fields, if there were any
    uploadedFiles.push(request.file.filenames);
    response.render('uploaded', {
        picture: request.file.filename
    })
})

app.get('/', (req, res) => {
    const path = './public/uploads';
    fs.readdir(path, function (err, items) {
        items = items.filter(item => item !== ".DS_Store");
        console.log(items);
        items.sort((a,b) => fs.statSync(path + '/' + b).mtimeMs - fs.statSync(path + '/' + a).mtimeMs)
        console.log(items);
        res.render('index', {
            title: 'KenzieGram',
            pictures: items
        })
    })
})

app.listen(port);